package asgn2Manifests;

import java.util.ArrayList;
import java.util.Stack;
import java.util.Arrays;

import asgn2Codes.ContainerCode;
import asgn2Containers.FreightContainer;
import asgn2Exceptions.ManifestException;

/**
 * A class for managing a container ship's cargo manifest.  It
 * allows freight containers of various types to be loaded and
 * unloaded, within various constraints.
 * <p>
 * In particular, the ship's captain has set the following rules
 * for loading of new containers:
 * <ol>
 * <li>
 * New containers may be loaded only if doing so does not exceed
 * the ship's weight limit.
 * </li>
 * <li>
 * New containers are to be loaded as close to the bridge as possible.
 * (Stack number zero is nearest the bridge.)
 * </li>
 * <li>
 * A new container may be added to a stack only if doing so will
 * not exceed the maximum allowed stack height.
 * <li>
 * A new container may be loaded only if a container with the same
 * code is not already on board.
 * </li>
 * <li>
 * Stacks of containers must be homogeneous, i.e., each stack must
 * consist of containers of one type (general,
 * refrigerated or dangerous goods) only.
 * </li>
 * </ol>
 * <p>
 * Furthermore, since the containers are moved by an overhead
 * crane, a container can be unloaded only if it is on top of
 * a stack.
 *  
 * @author CAB302 & Jamie Brearley (n9016228)
 * @version 1.0
 */
public class CargoManifest {
	/**
	 * Constructs a new cargo manifest in preparation for a voyage.
	 * When a cargo manifest is constructed the specific cargo
	 * parameters for the voyage are set, including the number of
	 * stack spaces available on the deck (which depends on the deck configuration
	 * for the voyage), the maximum allowed height of any stack (which depends on
	 * the weather conditions expected for the
	 * voyage), and the total weight of containers allowed onboard (which depends
	 * on the amount of ballast and fuel being carried).
	 * 
	 * @param numStacks the number of stacks that can be accommodated on deck
	 * @param maxHeight the maximum allowable height of any stack
	 * @param maxWeight the maximum weight of containers allowed on board 
	 * (in tonnes)
	 * @throws ManifestException if negative numbers are given for any of the
	 * parameters
	 */
	private Integer stacks;
	private Integer height;
	private Integer weight;
	private Integer currWeight;
	public ArrayList<Stack<FreightContainer>> containers;
	public ArrayList<Stack<FreightContainer>> eachStack;
		
	public CargoManifest(Integer numStacks, Integer maxHeight, Integer maxWeight)
	throws ManifestException {
		if(numStacks < 0){
			throw new ManifestException("numStacks can not be Negative");
		}
		if(maxHeight < 0){
			throw new ManifestException("maxHeight can not be Negative");
		}
		if(maxWeight < 0) {
			throw new ManifestException("maxWeight can not be Negative");
		}
		stacks = numStacks;
		height = maxHeight;
		weight = maxWeight;
		currWeight = 0;
		containers = new ArrayList<Stack<FreightContainer>>(stacks);
		
		for(int i = 0; i < stacks; i++){
		containers.add(new Stack<FreightContainer>());
		containers.get(i).add(null);
		}
	}

	/**
	 * Loads a freight container onto the ship, provided that it can be
	 * accommodated within the five rules set by the captain.
	 * 
	 * @param newContainer the new freight container to be loaded
	 * @throws ManifestException if adding this container would exceed
	 * the ship's weight limit; if- a container with the same code is
	 * already on board; or if no suitable space can be found for this
	 * container
	 */
	public void loadContainer(FreightContainer newContainer) throws ManifestException {
		// Not sure if containers.size takes stacks*height variable or just stacks variable
		
		if(currWeight + newContainer.getGrossWeight() > weight){
					throw new ManifestException("Adding this container would make the ship go over the weight limit");
				}
		
		for(int i = 0; i < containers.size(); i++){
			for(int j = 0; j < (containers.get(i).size()); j++){
				
				
				if(containers.get(i).get(j) != (null)) {
					if(containers.get(i).get(j).getCode() == (newContainer.getCode())){ 
					throw new ManifestException("A container with that code is already on board");
					}
				}
			}
		}
		
		for(int i = 0; i < containers.size(); i++){
			if(containers.get(i).get(0) != null){		
				if(containers.get(i).get(0).getClass() == newContainer.getClass()){
					containers.get(i).push(newContainer);
					return;
				}
			}
			else{
				containers.get(i).remove(0);
				containers.get(i).push(newContainer);
				return;
			}
		}
		throw new ManifestException("No suitable space can be found for this container");
	}

	/**
	 * Unloads a particular container from the ship, provided that
	 * it is accessible (i.e., on top of a stack).
	 * 
	 * @param containerId the code of the container to be unloaded
	 * @throws ManifestException if the container is not accessible because
	 * it's not on the top of a stack (including the case where it's not on board
	 * the ship at all)
	 */
	public void unloadContainer(ContainerCode containerId) throws ManifestException {
		for(int i = 0; i < containers.size(); i++){
			for(int j = 0; j < containers.get(i).size(); j++){
				if(containers.get(i).get(j) != (null)) {
					if(containers.get(i).get(j).getCode() == containerId){
						if(j+1 < height){
							if(containers.get(i).get(j+1) == null){
								currWeight -= containers.get(i).get(j).getGrossWeight();
								containers.get(i).remove(j);
							} else {
								throw new ManifestException("That container is not accessible");
							}
						} else{
							currWeight -= containers.get(i).get(j).getGrossWeight();
							containers.get(i).remove(j);
						}
					}
				}
			}
		}
		throw new ManifestException("That container is not on board");
	}

	
	/**
	 * Returns which stack holds a particular container, if any.  The
	 * container of interest is identified by its unique
	 * code.  Constant <code>null</code> is returned if the container is
	 * not on board.
	 * 
	 * @param queryContainer the container code for the container of interest
	 * @return the number of the stack with the container in it, or <code>null</code>
	 * if the container is not on board
	 */
	public Integer whichStack(ContainerCode queryContainer) {
		for(int i = 0; i < containers.size(); i++){
			for(int j = 0; j < containers.get(i).size(); j++){

				if(containers.get(i).get(j) != (null)) {
				
					if(containers.get(i).get(j).getCode() == queryContainer){
					return i;
					}
				}
			}
		}
	
		return null;
	}

	
	/**
	 * Returns how high in its stack a particular container is.  The container of
	 * interest is identified by its unique code.  Height is measured in the
	 * number of containers, counting from zero.  Thus the container at the bottom
	 * of a stack is at "height" 0, the container above it is at height 1, and so on.
	 * Constant <code>null</code> is returned if the container is
	 * not on board.
	 * 
	 * @param queryContainer the container code for the container of interest
	 * @return the container's height in the stack, or <code>null</code>
	 * if the container is not on board
	 */
	public Integer howHigh(ContainerCode queryContainer) {
		for(int i = 0; i < containers.size(); i++){
			for(int j = 0; j < containers.get(i).size(); j++){
				if (containers.get(i).get(j) != null){
				if(containers.get(i).get(j).getCode() == (queryContainer)){
					return j;
				} 
				}
			}
		}
		return null;
	}


	/**
	 * Returns the contents of a particular stack as an array,
	 * starting with the bottom most container at position zero in the array.
	 * 
	 * @param stackNo the number of the stack of interest
	 * @return the stack's freight containers as an array
	 * @throws ManifestException if there is no such stack on the ship
	 */
	public FreightContainer[] toArray(Integer stackNo) throws ManifestException {
		if(stackNo >= containers.size()){
			throw new ManifestException("There is no such stack on the ship");
		}
		
		//FreightContainer[] returnArray = (FreightContainer[]) containers.get(stackNo).toArray();
		FreightContainer[] returnArray = new FreightContainer[containers.get(stackNo).size()];
		for(int i = 0; i < containers.get(stackNo).size(); i++){
			if(containers.get(stackNo).get(i) != null){
				returnArray[i] = containers.get(stackNo).get(i);
			} 
		}
		
		
		return returnArray;
	}

	
	/* ***** toString methods added to support the GUI ***** */
	
	public String toString(ContainerCode toFind) {
		//Some variables here are used and not declared. You can work it out 
		String toReturn = "";
		for (int i = 0; i < containers.size(); ++i) {
			ArrayList<FreightContainer> currentStack = new ArrayList<FreightContainer>(containers.get(i));
			toReturn += "|";
			for (int j = 0; j < currentStack.size(); ++j) {
				if (toFind != null && currentStack.get(j).getCode().equals(toFind))
					toReturn += "|*" + currentStack.get(j).getCode().toString() + "*|";
				else
					toReturn += "| " + currentStack.get(j).getCode().toString() + " |";
			}
			if (currentStack.size() == 0)
				toReturn +=  "|  ||\n";
			else
				toReturn += "|\n";
		}
		return toReturn;
	}

	@Override
	public String toString() {
		return toString(null);
	}
}
