package asgn2GUI;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a JPanel in which graphical components are laid out to represent the cargo manifest.
 *
 * @author CAB302 & Jamie Brearley n9016228.
 */
public class CargoCanvas extends JPanel {

    private static final int WIDTH = 120;
    private static final int HEIGHT = 50;
    private static final int HSPACE = 10;
    private static final int VSPACE = 20;

    private final CargoManifest cargo;

    private ContainerCode toFind;

    /**
     * Constructor
     *
     * @param cargo The <code>CargoManifest</code> on which the graphics is based so that the
     * number of stacks and height can be adhered to.
     */
    public CargoCanvas(CargoManifest cargo) {
        this.cargo = cargo;
        setName("Canvas");
    }

    /**
     * Highlights a container.
     *
     * @param code ContainerCode to highlight.
     */
    public void setToFind(ContainerCode code) {
    	// ##################################################################################
    	Border redBorder = BorderFactory.createLineBorder(Color.RED,5);
    	// cargo.containers.get(cargo.whichStack(code)).get(cargo.howHigh(code)).setBorder(redBorder);
    }

    /**
     * Draws the containers in the cargo manifest on the Graphics context of the Canvas.
     *
     * @param g The Graphics context to draw on.
     */
    @Override
    public void paint(Graphics g) {
    	for(int i = 0; i < cargo.stacks; i++){
			for(int j = 0; j < cargo.containers.get(i).size(); j++){
				drawContainer(g, cargo.containers.get(i).get(j), i, j);
			}
		}
    }

    /**
     * Draws a container at the given location.
     *
     * @param g The Graphics context to draw on.
     * @param container The container to draw - the type determines the colour and ContainerCode is
     *            used to identify the drawn Rectangle.
     * @param x The x location for the Rectangle.
     * @param y The y location for the Rectangle.
     */
    private void drawContainer(Graphics g, FreightContainer container, int x, int y) {
    	int type = 0;
    	
    	if(container.equals(null))
    		type = 0;
    	else if(container.getClass().equals(GeneralGoodsContainer.class))
    		type = 1;
    	else if(container.getClass().equals(DangerousGoodsContainer.class))
    		type = 2;
    	else if(container.getClass().equals(RefrigeratedContainer.class))
    		type = 3;
    	
    	switch(type){
			case 0:
				break;
			case 1:
				g.setColor(Color.GREEN);
				g.drawRect((HSPACE+WIDTH)*x, (VSPACE+HEIGHT)*y, WIDTH, HEIGHT);
				g.fillRect((HSPACE+WIDTH)*x, (VSPACE+HEIGHT)*y, WIDTH, HEIGHT);
				break;
			case 2:
				g.setColor(Color.RED);
				g.drawRect((HSPACE+WIDTH)*x, (VSPACE+HEIGHT)*y, WIDTH, HEIGHT);
				g.fillRect((HSPACE+WIDTH)*x, (VSPACE+HEIGHT)*y, WIDTH, HEIGHT);
				break;
			case 3:
				g.setColor(Color.BLUE);
				g.drawRect((HSPACE+WIDTH)*x, (VSPACE+HEIGHT)*y, WIDTH, HEIGHT);
				g.fillRect((HSPACE+WIDTH)*x, (VSPACE+HEIGHT)*y, WIDTH, HEIGHT);
				break;
		} 
    }
}
