package asgn2GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import asgn2Exceptions.CargoException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a dialog box allowing the user to enter parameters for a new <code>CargoManifest</code>.
 *
 * @author CAB302 & Jamie Brearley n9016228
 */
public class ManifestDialog extends AbstractDialog {

    private static final int HEIGHT = 150;
    private static final int WIDTH = 250;

    private JTextField txtNumStacks;
    private JTextField txtMaxHeight;
    private JTextField txtMaxWeight;
    private FlowLayout flowLayout;

    private CargoManifest manifest;

    /**
     * Constructs a modal dialog box that gathers information required for creating a cargo
     * manifest.
     *
     * @param parent the frame which created this dialog box.
     */
    private ManifestDialog(JFrame parent) {
        super(parent, "Create Manifest", WIDTH, HEIGHT);
        setName("New Manifest");
        setResizable(false);
        manifest = null;
    }

    /**
     * @see AbstractDialog.createContentPanel()
     */
    @Override
    protected JPanel createContentPanel() {
    	JPanel toReturn = new JPanel();
        flowLayout = new FlowLayout();
    	toReturn.setLayout(flowLayout);
    	flowLayout.setAlignment(FlowLayout.CENTER);
    	
        txtNumStacks = createTextField(8, "Number of Stacks");
        txtMaxHeight = createTextField(8, "Maximum Height");
        txtMaxWeight = createTextField(8, "Maximum Weight");

        toReturn.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
    	
        addToPanel(toReturn, new JLabel("Number of Stacks"), c, 0, 0, 50, 20);
        addToPanel(toReturn, txtNumStacks, c, 50, 0, 50, 20);
        addToPanel(toReturn, new JLabel("Maximum Height"), c, 0, 50, 50, 20);
        addToPanel(toReturn, txtMaxHeight, c, 50, 50, 50, 20);
        addToPanel(toReturn, new JLabel("Maximum Weight"), c, 0, 100, 50, 20);
        addToPanel(toReturn, txtMaxWeight, c, 50, 100, 50, 20);
    	
    	c.fill = GridBagConstraints.HORIZONTAL;    	
    	
    	return toReturn;
    }

    /*
     * Factory method to create a named JTextField
     */
    private JTextField createTextField(int numColumns, String name) {
        JTextField text = new JTextField();
        text.setColumns(numColumns);
        text.setName(name);
        return text;
    }

    @Override
    protected boolean dialogDone() {
    	try {
    		Integer numStacks = Integer.parseInt(txtNumStacks.getText());
        	Integer maxHeight = Integer.parseInt(txtMaxHeight.getText());
        	Integer maxWeight = Integer.parseInt(txtMaxWeight.getText());
        	
            manifest = new CargoManifest(numStacks, maxHeight, maxWeight);
            return true;
		} catch(ManifestException e){
			JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
		} catch(NumberFormatException e){
			JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
		}
    	return false;
    }

    /**
     * Shows the <code>ManifestDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>CargoManifest</code> instance with valid values.
     */
    public static CargoManifest showDialog(JFrame parent) {
    	ManifestDialog md = new ManifestDialog(parent);
    	md.setSize(300, 150);
    	md.add(md.createContentPanel(), BorderLayout.NORTH);
    	md.add(md.createDialogControls(), BorderLayout.SOUTH);
    	md.setVisible(true);
    	return md.manifest;
    }
}
