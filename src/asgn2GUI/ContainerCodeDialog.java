package asgn2GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
// import java.awt.event.ActionEvent;
// import java.awt.event.ActionListener;




import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Exceptions.CargoException;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;

/**
 * Creates a dialog box allowing the user to enter a ContainerCode.
 *
 * @author CAB302 & Jamie Brearley n9016228
 */
public class ContainerCodeDialog extends AbstractDialog {

    private final static int WIDTH = 250;
    private final static int HEIGHT = 120;

    private JTextField txtCode;
    private JLabel lblErrorInfo;

    private ContainerCode code;

    /**
     * Constructs a modal dialog box that requests a container code.
     *
     * @param parent the frame which created this dialog box.
     */
    private ContainerCodeDialog(JFrame parent) {
        super(parent, "Container Code", WIDTH, HEIGHT);
        setName("Container Dialog");
        setResizable(true);
    }

    /**
     * @see AbstractDialog.createContentPanel()
     */
    @Override
    protected JPanel createContentPanel() {
        JPanel toReturn = new JPanel();
        toReturn.setLayout(new GridBagLayout());
        
        txtCode = new JTextField();
        txtCode.setColumns(20);
        
        GridBagConstraints constraints = new GridBagConstraints();
        
        addToPanel(toReturn, new JLabel("Container Code"), constraints, 0, 0, 2, 2);
        addToPanel(toReturn, txtCode, constraints, 0, 4, 50, 20);

        // Defaults
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 100;
        constraints.weighty = 100;

        txtCode = new JTextField();
        txtCode.setColumns(11);
        txtCode.setName("Container Code");
        txtCode.getDocument().addDocumentListener(new DocumentListener() {

            @Override
            public void removeUpdate(DocumentEvent e) {
                validate();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                validate();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                validate();
            }
            /*
             * Attempts to validate the ContainerCode entered in the Container Code text field.
             */
            private void validate() {
            	try {
					code = new ContainerCode(txtCode.getText());
					dialogDone();
				} catch (InvalidCodeException e) {
					System.out.println("Invalid Code in ContainerCodeDialog");
				}
            }
        });
        
        
        
        return toReturn;
    }

    @Override
    protected boolean dialogDone() {
    	try {
     		code = new ContainerCode(txtCode.getText());
            return true;
 		} catch (InvalidCodeException e) {
 			JOptionPane.showMessageDialog(this, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
 		}     	
     	return false;
    }

    /**
     * Shows the <code>ManifestDialog</code> for user interaction.
     *
     * @param parent - The parent <code>JFrame</code> which created this dialog box.
     * @return a <code>ContainerCode</code> instance with valid values.
     */
    public static ContainerCode showDialog(JFrame parent) {
    	ContainerCodeDialog ccd = new ContainerCodeDialog(parent);
    	ccd.setSize(300, 150);
    	ccd.add(ccd.createContentPanel(), BorderLayout.NORTH);
    	ccd.add(ccd.createDialogControls(), BorderLayout.SOUTH);
    	ccd.setVisible(true);
    	return ccd.code;
    }
}
