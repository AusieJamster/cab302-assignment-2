package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 */


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;

/**
 * @author Jamie Brearley (n9016228)
 *
 */

public class ContainerTests {
	private DangerousGoodsContainer dgc;
	private GeneralGoodsContainer ggc;
	private RefrigeratedContainer fc;
	private ContainerCode code;
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 *  
	 */
	@Test
	public void codeValid() throws InvalidCodeException {
		code = new ContainerCode("CSQU3054389");
	}
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidCodeException.class)
	public void codeInvalid() throws InvalidCodeException {
		code = new ContainerCode("CSQU3054385");
	}
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidCodeException.class)
	public void codeTooLittleNumbers() throws InvalidCodeException {
		code = new ContainerCode("CSQU305435");
	}
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidCodeException.class)
	public void codeTooManyNumbers() throws InvalidCodeException {
		code = new ContainerCode("CSQU11111116");
	}
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidCodeException.class)
	public void codeTooManyLetters() throws InvalidCodeException {
		code = new ContainerCode("AAAAU53056");
	}
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidCodeException.class)
	public void codeTooLittleLetters() throws InvalidCodeException {
		code = new ContainerCode("U554243059");
	}
	
	/**
	 * Test method for {@link asgn2Codes.ContainerCode#ContainerCode(String)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidCodeException.class)
	public void codeTooSmall() throws InvalidCodeException {
		code = new ContainerCode("CSQU305435");
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcFirstIntegerBoundary() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 31, 1);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcFirstIntegerBoundary2() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 3, 1);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcFirstIntegerNull() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 0, 1);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcFirstIntegerNegative() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, -1, 1);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcSecondIntegerBoundary() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 4, 10);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcSecondIntegerNull() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 4, 0);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcSecondIntegerNegative() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 4, -1);
	}

	/**
	 * Test method for {@link asgn2Containers.DangerousGoodsContainer#DangerousGoodsContainer(ContainerCode, Integer, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void dgcSecondIntegerBoundary2() throws InvalidContainerException {
		dgc = new DangerousGoodsContainer(code, 4, 0);
	}

	/**
	 * Test method for {@link asgn2Containers.GeneralGoodsContainer#GeneralGoodsContainer(ContainerCode, Integer)}.
	 * @throws InvalidContainerException
	 */
	@Test(expected=InvalidContainerException.class)
	public void ggcWeightBoundary() throws InvalidContainerException {
		ggc = new GeneralGoodsContainer(code, 31);
	}

	/**
	 * Test method for {@link asgn2Containers.GeneralGoodsContainer#GeneralGoodsContainer(ContainerCode, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void ggcWeightBoundary2() throws InvalidContainerException {
		ggc = new GeneralGoodsContainer(code, 3);
	}

	/**
	 * Test method for {@link asgn2Containers.GeneralGoodsContainer#GeneralGoodsContainer(ContainerCode, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void ggcWeightNull() throws InvalidContainerException {
		ggc = new GeneralGoodsContainer(code, 0);
	}

	/**
	 * Test method for {@link asgn2Containers.GeneralGoodsContainer#GeneralGoodsContainer(ContainerCode, Integer)}.
	 * @throws InvalidContainerException 
	 */
	@Test(expected=InvalidContainerException.class)
	public void WeightNegative() throws InvalidContainerException {
		ggc = new GeneralGoodsContainer(code, -1);
	}
	
	/**
	 * Test method for {@link asgn2Containers.RefrigeratedContainer#RefrigeratedContainer(ContainerCode, Integer, Integer)}
	 * @throws InvalidContainerException
	 */
	@Test(expected=InvalidContainerException.class)
	public void fcWeightBoundary() throws InvalidContainerException {
		fc = new RefrigeratedContainer(code, 31, 1);
	}
	
	/**
	 * Test method for {@link asgn2Containers.RefrigeratedContainer#RefrigeratedContainer(ContainerCode, Integer, Integer)}
	 * @throws InvalidContainerException
	 */
	@Test(expected=InvalidContainerException.class)
	public void fcWeightBoundary2() throws InvalidContainerException {
		fc = new RefrigeratedContainer(code, 3, 1);
	}
	
	/**
	 * Test method for {@link asgn2Containers.RefrigeratedContainer#RefrigeratedContainer(ContainerCode, Integer, Integer)}
	 * @throws InvalidContainerException
	 */
	@Test(expected=InvalidContainerException.class)
	public void fcWeightNull() throws InvalidContainerException {
		fc = new RefrigeratedContainer(code, 0, 1);
	}
	
	/**
	 * Test method for {@link asgn2Containers.RefrigeratedContainer#RefrigeratedContainer(ContainerCode, Integer, Integer)}
	 * 
	 */
	@Test
	public void fcTempNull() throws InvalidContainerException {
		fc = new RefrigeratedContainer(code, 4, 0);
	}
	
	/**
	 * Test method for {@link asgn2Containers.RefrigeratedContainer#RefrigeratedContainer(ContainerCode, Integer, Integer)}
	 * @throws InvalidContainerException
	 */
	@Test(expected=InvalidContainerException.class)
	public void fcWeightNegative() throws InvalidContainerException {
		fc = new RefrigeratedContainer(code, -1, 1);
	}
}