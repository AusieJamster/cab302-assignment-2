package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 * QUTU7200318
 * IBMU4882351
 */

import org.junit.Before;
import org.junit.Test;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;
import static org.junit.Assert.*;
import java.util.Arrays;

/**
 * @author Jonathon Jackson N9192883
 **/

public class ManifestTests {
	
	private CargoManifest cm;
	private ContainerCode cc;
	private ContainerCode cc2;
	private ContainerCode cc3;
	private GeneralGoodsContainer ggc;
	private GeneralGoodsContainer ggc2;
	private GeneralGoodsContainer ggc3;
	
	
	/**
	 * Checks that a ManifestException is thrown when there are negative stacks.
	 * @throws ManifestException 
	 */
	@Test(expected=ManifestException.class)
	public void checkNegativeStacks() throws ManifestException {
		cm = new CargoManifest(-1,1,1);
	}
	
	/**
	 * Checks that a ManifestException is thrown when the height is negative.
	 * @throws ManifestException 
	 */
	@Test(expected=ManifestException.class)
	public void checkNegativeHeight() throws ManifestException {
		cm = new CargoManifest(1,-1,1);
	}

	/**
	 * Checks that a ManifestException is thrown when the weight is negative.
	 * @throws ManifestException 
	 */
	@Test(expected=ManifestException.class)
	public void checkNegativeWeight() throws ManifestException {
		cm = new CargoManifest(1,1,-1);
	}

	/**
	 * Checks that CargoManifest() works with null stacks (lower boundary).
	 */
	@Test
	public void checkNullStacks() throws ManifestException {
		cm = new CargoManifest(0,1,1);
	}

	/**
	 * Checks that CargoManifest() works with null height (lower boundary).
	 */
	@Test
	public void checkNullHeight() throws ManifestException {
		cm = new CargoManifest(1,0,1);
	}

	/**
	 * Checks that CargoManifest() works with null weight (lower boundary).
	 */
	@Test
	public void checkNullWeight() throws ManifestException {
		cm = new CargoManifest(1,1,0);
	}

	/**
	 * Checks that CargoManifest() works with a generic input.
	 */
	@Test
	public void CargoManifestTest() throws ManifestException {
		cm = new CargoManifest(2,3,30);
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Checks that loadContainer() successfully loads the container 
	 * if given the correct parameters.
	 */
	@Test
	public void SuccessfulLoadContainer() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(3,4,10);
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.loadContainer(ggc);
		int whichStack = cm.whichStack(cc);
		int howHigh = cm.howHigh(cc);
		FreightContainer [] returnedArray = (cm.toArray(whichStack));
		assertEquals(ggc, returnedArray[howHigh]);
	}
	
	/**
	 * Checks that loadContainer() throws a ManifestException if 
	 * adding this container would exceed the ships weight limit.
	 */
	@Test(expected=ManifestException.class)
	public void UnsuccessfulLoadContainerOverWeight() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(2,4,0);
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.loadContainer(ggc);
	}
	
	/**
	 * Checks that loadContainer() throws a ManifestException if 
	 * adding this container is already on board.
	 */
	@Test(expected=ManifestException.class)
	public void UnsuccessfulLoadContainerAlreadyOnBoard() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(2,3,20);
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.loadContainer(ggc); //should pass
		cm.loadContainer(ggc); //should fail
	}
	
	/**
	 * Checks that loadContainer() throws a ManifestException if 
	 * there is no space for the container on board.
	 */
	@Test(expected=ManifestException.class)
	public void UnsuccessfulLoadContainerNoSpace() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(0,0,20);
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.loadContainer(ggc);
	}

/////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Checks that unloadContainer()  successfully unloads the container 
	 * if given the correct parameters.
	 */
	@Test
	public void SuccessfulUnloadContainer() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20);
		cc = new ContainerCode("INKU2633836");
		cc2 = new ContainerCode("KOCU8090115");
		ggc = new GeneralGoodsContainer(cc, 5);
		ggc2 = new GeneralGoodsContainer(cc2, 5);
		
		cm.loadContainer(ggc);
		cm.loadContainer(ggc2);
		int whichStack = cm.whichStack(cc2);
		
		FreightContainer [] loadedContainerArray = (cm.toArray(whichStack));
		cm.unloadContainer(cc2);	
		FreightContainer[] unloadedContainerArray = (cm.toArray(whichStack));

		//assertNotEquals(loadedContainerArray.toString(), unloadedContainerArray.toString());
		
	}
	
	/**
	 * Checks that unloadContainer() throws a ManifestException if 
	 * the container is not on board.
	 */
	@Test(expected=ManifestException.class)
	public void UnsuccessfulUnLoadContainerNotOnBoard() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20);
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.unloadContainer(cc);
		
	}
	
	/**
	 * Checks that unloadContainer() throws a ManifestException if 
	 * the container is not on the top of the stack.
	 */
	@Test(expected=ManifestException.class)
	public void UnsuccessfulUnLoadContainerNotOnTopOfStack() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20); //only 1 stack to load containers to
		cc = new ContainerCode("INKU2633836");
		cc2 = new ContainerCode("KOCU8090115");
		cc3 = new ContainerCode("MSCU6639871");
		ggc = new GeneralGoodsContainer(cc, 5);
		ggc2 = new GeneralGoodsContainer(cc2, 5);
		ggc3 = new GeneralGoodsContainer(cc3, 5);
		
		cm.loadContainer(ggc);
		cm.loadContainer(ggc2);
		cm.loadContainer(ggc3);
		cm.unloadContainer(cc);//the Container Code for 'ggc' (bottom of stack)
		
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Checks that whichStack() returns null if 
	 * the container is not on board.
	 */
	@Test
	public void WhichStackReturnNullNotOnBoard() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20); 
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.whichStack(cc);
		
	}
	
	/**
	 * Checks that whichStack() returns the correct stack number  
	 * for the container.
	 */
	@Test
	public void WhichStackReturnsCorrectStack() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20);
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.loadContainer(ggc);
		int correctStack = 0;
		int stackNumber = cm.whichStack(cc);
		assertTrue(stackNumber == correctStack); //stack "0" is the first stack which is where the container "ggc" was stored
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Checks that howHigh() returns null if 
	 * the container is not on board.
	 */
	@Test
	public void HowHighReturnNullNotOnBoard() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20); 
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		
		cm.howHigh(cc);
		
	}
	
	/**
	 * Checks that howHigh() returns the correct height  
	 * of the container within its stack.
	 */
	@Test
	public void HowHighReturnsCorrectStack() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20); 
		cc = new ContainerCode("INKU2633836");
		ggc = new GeneralGoodsContainer(cc, 5);
		cm.loadContainer(ggc);
		int correctHeight = 0;
		int height = cm.howHigh(cc);
		assertTrue(height == correctHeight); //"0" is the height of the bottom container which is where "ggc" is stored
	}
	
/////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Checks that FreightContainer[] toArray() returns a ManifestException  
	 * when there is no such stack on ship.
	 */
	@Test(expected=ManifestException.class)
	public void ToArrayThrowsManifestException() throws ManifestException{
		cm = new CargoManifest(1,3,20); 
		FreightContainer [] returnedArray = (cm.toArray(2));
		
	}
	
	/**
	 * Checks that FreightContainer[] toArray() returns the correct array  
	 * of the containers within its stack.
	 */
	@Test
	public void ToArrayReturnsCorrectArray() throws ManifestException , InvalidCodeException , InvalidContainerException {
		cm = new CargoManifest(1,3,20); 
		cc = new ContainerCode("INKU2633836");
		cc2 = new ContainerCode("KOCU8090115");
		cc3 = new ContainerCode("MSCU6639871");
		ggc = new GeneralGoodsContainer(cc, 5);
		ggc2 = new GeneralGoodsContainer(cc2, 5);
		ggc3 = new GeneralGoodsContainer(cc3, 5);
		
		FreightContainer[] expectedArray = {ggc , ggc2 , ggc3};
		
		cm.loadContainer(ggc);
		cm.loadContainer(ggc2);
		cm.loadContainer(ggc3);
		
		FreightContainer [] returnedArray = (cm.toArray(cm.whichStack(cc)));
		
		assertArrayEquals(expectedArray, returnedArray);	
	}
	
}





